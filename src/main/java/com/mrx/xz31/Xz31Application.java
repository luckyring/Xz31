package com.mrx.xz31;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xz31Application {

    public static void main(String[] args) {
        SpringApplication.run(Xz31Application.class, args);
    }

}
